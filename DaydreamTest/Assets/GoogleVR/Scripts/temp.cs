﻿using UnityEngine;
using System.Collections;

public class temp : GvrBasePointer {

	public float maxReticleDistance = 2.5f;

	// Use this for initialization
	void Awake() {
	}
	
	void Update () {
	
	}

	#region implemented abstract members of GvrBasePointer

	public override void OnInputModuleEnabled ()
	{
	}

	public override void OnInputModuleDisabled ()
	{
	}

	public override void OnPointerEnter (GameObject targetObject, Vector3 intersectionPosition, Ray intersectionRay, bool isInteractive)
	{
		targetObject.GetComponent<Renderer> ().material.color = Color.red;
	}

	public override void OnPointerHover (GameObject targetObject, Vector3 intersectionPosition, Ray intersectionRay, bool isInteractive)
	{
	}

	public override void OnPointerExit (GameObject targetObject)
	{
		targetObject.GetComponent<Renderer> ().material.color = Color.green;
	}

	public override void OnPointerClickDown ()
	{
		throw new System.NotImplementedException ();
	}

	public override void OnPointerClickUp ()
	{
	}

	public override float GetMaxPointerDistance ()
	{
		return maxReticleDistance;
	}

	public override void GetPointerRadius (out float innerRadius, out float outerRadius)
	{
		innerRadius = 0.0f;
		outerRadius = 0.0f;
	}

	#endregion
}
