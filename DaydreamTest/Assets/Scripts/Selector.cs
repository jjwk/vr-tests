﻿using UnityEngine;
using System.Collections;

public class Selector : MonoBehaviour {

	private bool selected;
	private bool hovering;

	public Selector () {
		selected = false;
		hovering = false;
	}

	public void Select() {
		selected = true;
	}

	public void Deselect() {
		selected = false;
	}

	public void HoverSelect() {
		hovering = true;
	}

	public void HoverDeselect() {
		hovering = false;
	}
		
	public bool isSelected {
		get {
			return selected;
		}
	}

	public bool isHovering {
		get {
			return hovering;
		}
	}
}
