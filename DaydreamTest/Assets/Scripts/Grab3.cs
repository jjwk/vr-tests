﻿using UnityEngine;
using System.Collections;

public class Grab3 : MonoBehaviour, IGvrGazeResponder
{

	public GameObject controller;

	private Vector3 distanceVec;
	private float distance;
	private bool grabbing = false;
	private bool touching = false;
	private float touchDiff = 0f;
	private float touchScale = .35f;
	private Vector2 touchPos;
	private RaycastHit pointerHit;

	// Use this for initialization
	void Start ()
	{
		GetComponent<Renderer> ().material.color = Color.blue;
		this.gameObject.GetComponent<Rigidbody>().mass	 *= .5f;
	}
		
	// Update is called once per frame
	void Update ()
	{
		//Swipe detection
		if (GvrController.IsTouching && !touching && !GvrController.ClickButton) {
			touching = true;
			touchPos = GvrController.TouchPos;
		}

		if (touching) {
			touchDiff += touchPos.y - GvrController.TouchPos.y;
		}

		if (!GvrController.IsTouching) {
			touching = false;
		}

		if (grabbing) {
			Quaternion ori = GvrController.Orientation;
			Vector3 controllerHeading = (ori * Vector3.forward);
		
			Ray grabber = new Ray (controller.transform.position, controllerHeading);
			Vector3 newPos = grabber.GetPoint (distance + touchDiff*touchScale);
//			newPos.x += 10;
			newPos.y -= 3.5f;

		
			this.gameObject.transform.position = newPos;

//			FollowTargetWitouthRotation (newPos, .5f, 10000f);

			GetComponent<Renderer> ().material.color = Color.red;
		}

		if (GvrController.ClickButtonUp) {
//			GetComponent<Renderer> ().material.color = Color.green;

			if (!grabbing) {
				grabbing = true;
				distance = Vector3.Distance (this.gameObject.transform.position, controller.transform.position);
				GetComponent<Renderer> ().material.color = Color.green;
			} else {
				grabbing = false;
				GetComponent<Renderer> ().material.color = Color.red;
			}
		}
	}

	void castRay () {
		Quaternion ori = GvrController.Orientation;
		Vector3 heading = (ori * Vector3.forward);
		Physics.Raycast (controller.transform.position, heading, out pointerHit);
	}

	void FollowTargetWitouthRotation(Vector3 target, float distanceToStop, float speed)
	{
		var direction = Vector3.zero;
		if(Vector3.Distance(transform.position, target) > distanceToStop)
		{
//			direction = target - transform.position;
			transform.LookAt(target);
			GetComponent<Rigidbody>().AddRelativeForce(direction.normalized * speed, ForceMode.Force);
		}
	}

	public void OnGazeEnter() {

	}

	public void OnGazeExit() {

	}

	public void OnGazeTrigger() {

	}


}
