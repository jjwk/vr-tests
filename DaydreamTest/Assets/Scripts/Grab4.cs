﻿using UnityEngine;
using System.Collections;

public class Grab4 : GvrBasePointer {

	private float maxReticleDistance = 2.5f;

	private GameObject grabbingObject;
	private float grabDistance;
	private bool activeHover = false;
	private bool grabbing = false;
	private bool prevGrabbing = false;
	private bool touching = false;
	private float touchDiff = 0f;
	private float touchScale = .3f;
	private float velocityScale = 5f;
	private Vector2 touchPos;
	private Vector3 grabOffset;

	// Use this for initialization
	void Awake() {
	}

	void LateUpdate () {
		if (grabbing) {
			Quaternion ori = GvrController.Orientation;
			Vector3 heading = ori * Vector3.forward;

			Ray grabber = new Ray (transform.position, heading);
			Vector3 pointerPos = grabber.GetPoint (grabDistance + touchDiff * touchScale);

			if (!prevGrabbing) {
				prevGrabbing = true;
				grabOffset = pointerPos - grabbingObject.transform.position;
			}

			Vector3 targetPos = pointerPos - grabOffset;

//			newPos.y -= 3;
//			grabbingObject.transform.position = targetPos;
			Vector3 posDelta = pointerPos - grabbingObject.transform.position;
			grabbingObject.GetComponent<Rigidbody>().velocity = posDelta*velocityScale;
		}

		//Swipe detection
		if (GvrController.IsTouching && !touching && !GvrController.ClickButton && grabbing) {
			touching = true;
			touchPos = GvrController.TouchPos;
		}

		if (touching && grabbing) {
			touchDiff += touchPos.y - GvrController.TouchPos.y;
		}

		if (!GvrController.IsTouching) {
			touching = false;
		}
	}

	void setEmission(GameObject obj, Color color) {
		Color baseColor = color; //Replace this with whatever you want for your base color at emission level '1'
		float emission = .5f;
		Color finalColor = baseColor * Mathf.LinearToGammaSpace (emission);
		obj.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", finalColor);
	}

	#region implemented abstract members of GvrBasePointer

	public override void OnInputModuleEnabled ()
	{
	}

	public override void OnInputModuleDisabled ()
	{
	}

	public override void OnPointerEnter (GameObject targetObject, Vector3 intersectionPosition, Ray intersectionRay, bool isInteractive)
	{
		if (!grabbing) {
			grabbingObject = targetObject;
//			setEmission(grabbingObject, Color.green);
			activeHover = true;
			targetObject.GetComponent<Selector> ().HoverSelect();
		}
	}

	public override void OnPointerHover (GameObject targetObject, Vector3 intersectionPosition, Ray intersectionRay, bool isInteractive)
	{
	}

	public override void OnPointerExit (GameObject targetObject)
	{
//		targetObject.GetComponent<Renderer> ().material.color = Color.green;
//			setEmission (targetObject, Color.white);
		targetObject.GetComponent<Selector> ().HoverDeselect();

		activeHover = false;
	}

	public override void OnPointerClickDown ()
	{
		
	}

	public override void OnPointerClickUp ()
	{
		if (activeHover && !grabbing) {
			grabbing = true;
			grabDistance = Vector3.Distance (this.gameObject.transform.position, grabbingObject.transform.position);

			if (grabbingObject.GetComponent<Selector> () != null) {
				grabbingObject.GetComponent<Selector> ().Select ();
			}

//			setEmission(grabbingObject, Color.red);

		} else if(grabbing) {
//			setEmission(grabbingObject, Color.white);

			if (grabbingObject.GetComponent<Selector> () != null) {
				grabbingObject.GetComponent<Selector> ().Deselect ();
				grabbingObject.GetComponent<Selector> ().HoverDeselect();
			}

			grabbing = false;
			touchDiff = 0;
			prevGrabbing = false;
			grabOffset = Vector3.zero;
			grabbingObject = null;


		}
	}

	public override float GetMaxPointerDistance ()
	{
		return maxReticleDistance;
	}

	public override void GetPointerRadius (out float innerRadius, out float outerRadius)
	{
		innerRadius = 0.0f;
		outerRadius = 0.0f;
	}

	#endregion
}
