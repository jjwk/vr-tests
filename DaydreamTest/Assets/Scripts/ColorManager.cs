﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Selector))]
public class ColorManager : MonoBehaviour {

	Selector selector;

	public Color hoverColor = Color.green;
	public Color selectedColor = Color.red;
	public Color defaultColor = Color.white;

	// Use this for initialization
	void Awake () {
		selector = GetComponent<Selector> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (selector.isHovering && !selector.isSelected) {
			setEmission (hoverColor);
		} else if (selector.isSelected) {
			setEmission (selectedColor);
		} else {
			setEmission (defaultColor);
		}
	}

	void setEmission(Color color) {
		Color baseColor = color; //Replace this with whatever you want for your base color at emission level '1'
		float emission = .5f;
		Color finalColor = baseColor * Mathf.LinearToGammaSpace (emission);
		GetComponent<Renderer> ().material.SetColor ("_EmissionColor", finalColor);
	}
}
