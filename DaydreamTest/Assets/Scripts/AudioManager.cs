﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(GvrAudioSource), typeof(Selector))]
public class AudioManager : MonoBehaviour {

	Selector selector;
	GvrAudioSource audio;

	// Use this for initialization
	void Awake () {
		selector = GetComponent<Selector> ();
		audio = GetComponent<GvrAudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(selector.isSelected && !audio.isPlaying) {
			audio.Play ();
		}

		if (!selector.isSelected && audio.isPlaying) {
			audio.Pause ();
		}
	}
}
