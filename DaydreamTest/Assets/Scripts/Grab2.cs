﻿using UnityEngine;
using System.Collections;

public class Grab2 : MonoBehaviour, IGvrGazeResponder
{

	//ATTACH TO CONTROLLER

	private Vector3 distanceVec;
	private float distance;
	private bool grabbing = false;
	private RaycastHit pointerHit;
	private GameObject target;


	// Use this for initialization
	void Start ()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (GvrController.ClickButtonDown) {
			if (castRay()) {
				grabbing = true;
				target = pointerHit.collider.gameObject;
				distance = pointerHit.distance;
			}
		}

		if (GvrController.ClickButton && grabbing) {
			target.GetComponent<Renderer> ().material.color = Color.red;

			Quaternion ori = GvrController.Orientation;
			Vector3 controllerHeading = (ori * Vector3.forward);
			Ray grabber = new Ray (this.gameObject.transform.position, controllerHeading);
			target.transform.position = grabber.GetPoint (distance);
		}

		if (GvrController.ClickButtonUp) {
			target.GetComponent<Renderer> ().material.color = Color.green;

			grabbing = false;
		}


		/*
		if (GvrController.ClickButtonDown) {
			Quaternion ori = GvrController.Orientation;
			Vector3 heading = (ori * Vector3.forward);

			distanceVec = this.gameObject.transform.position - controller.transform.position;
			distance = distanceVec.magnitude;
			GetComponent<Renderer> ().material.color = Color.red;
		}
		if (GvrController.ClickButton) {
			Quaternion ori = GvrController.Orientation;
			Vector3 controllerHeading = (ori * Vector3.forward);

//			Vector3 controllerHeading = controller.transform.position;
//			controllerHeading.Normalize ();
			Vector3 newPos = controllerHeading  * distance;
			this.gameObject.transform.position = newPos;
			this.gameObject.transform.localRotation = ori;

			GetComponent<Renderer> ().material.color = Color.red;
		}

		if (GvrController.ClickButtonUp) {
			GetComponent<Renderer> ().material.color = Color.green;
		}
		*/
	}

	bool castRay () {
		Quaternion ori = GvrController.Orientation;
		Vector3 heading = (ori * Vector3.forward);
		if (Physics.Raycast (this.gameObject.transform.position, heading, out pointerHit)) {
			return true;
		}
		return false;
	}

	public void OnGazeEnter() {
		
	}

	public void OnGazeExit() {
		 
	}

	public void OnGazeTrigger() {
		
	}


}
